import base64

with open('rawdata.txt', 'rb') as f:
    with open('file.zip', 'wb') as out:
        data = base64.b64decode(f.read())
        out.write(data)

print('look end of zip file in an hex editor to find the zip password')
