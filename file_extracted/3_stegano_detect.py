import png


def enhance(byte, bit=0):
    return (byte & (1<<bit)) << (7 - bit)


r = png.Reader(filename="lsb_RGB.png")
width, height, data, metadata = r.read_flat()
print("file read:", metadata)

for lsb in range(2):
    for color in range(3):
        filename = f"detect-{'rgb'[color]}-{lsb}.png"
        out = open(filename, "wb")
        d = png.Writer(width=width, height=height)
        detect_data = []
        for i, byte in enumerate(data):
            if i % 3 == color:
                detect_data.append(enhance(byte, lsb))
            else:
                detect_data.append(0)
        d.write_array(out, detect_data)
        out.close()
        print(f"{filename} done")
