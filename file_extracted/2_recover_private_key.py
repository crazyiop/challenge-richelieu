from Crypto.PublicKey import RSA, pubkey
from itertools import product
from copy import copy


def find_correct_prime(n, replacements):
    def _to_bigint(lst):
        res = 0
        for p in lst:
            res *= 256
            res += p
        return res

    def _test_changes(data, change_pool, bool_lst):
        cp = copy(data)
        for (ori, start), change in zip(change_pool, bool_lst):
            if change:
                cp[start] = ori

        return divmod(n, _to_bigint(cp))

    f = open("prime.txt", "r")
    data_in = "".join(
        [line.strip() for line in f.readlines()[1:]]
    )  # Header is excluded

    # Work with numbers
    data_in = [int(p, 16) for p in data_in.split(":")]
    replacements = [(int(a, 16), int(b, 16)) for a, b in replacements]

    change_pool = []
    for ori, to in replacements:
        start = -1
        while True:
            try:
                start = data_in.index(to, start + 1)
            except ValueError:
                break
            change_pool.append((ori, start))

    for prod in product([True, False], repeat=len(change_pool)):
        q, r = _test_changes(data_in, change_pool, prod)
        if not r:
            p = n // q
            print("primes factors found")
            return p, q

    return None, None


# Custom subclass to override key generation by directly feeding the prime number found
class RSAFromPrime(RSA.RSAImplementation):
    def generate(self, e, p, q):
        if p > q:
            p, q = q, p
        n = p * q
        d = pubkey.inverse(e, (p - 1) * (q - 1))
        u = pubkey.inverse(p, q)
        key = self._math.rsa_construct(n, e, d, p, q, u)
        return RSA._RSAobj(self, key)


def generate_private_key(e, p, q, filename="priv.key"):
    rsa = RSAFromPrime()
    key = rsa.generate(e, p, q)
    with open(filename, "wb") as export_file:
        export_file.write(key.exportKey("PEM"))
    print(f"{filename} created")


if __name__ == "__main__":
    with open("public.key", "r") as pub:
        public_key = RSA.importKey(pub.read())
    n = public_key.n
    e = public_key.e

    # sed executed from .bash_history
    replacements = [
        ("7f", "fb"),
        ("e1", "66"),
        ("f4", "12"),
        ("16", "54"),
        ("a4", "57"),
        ("b5", "cd"),
    ]

    p, q = find_correct_prime(n, replacements)
    generate_private_key(e, p, q)

    print("You can now run:")
    print(
        "\topenssl rsautl -decrypt -in motDePasseGPG.txt.enc -out motDePasseGPG.txt -inkey priv.key"
    )
    print(
        "and use the password to run:\n\tgpg --output lsb_RGB.png --decrypt lsb_RGB.png"
    )
