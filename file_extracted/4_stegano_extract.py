import os
import png
from array import array


if not os.path.exists("png.array"):
    r = png.Reader(filename="lsb_RGB.png")
    width, height, data, metadata = r.read_flat()
    print(metadata)
    fp = open("png.array", "wb")
    data.tofile(fp)
    fp.close()
    print("raw png data written to png.array for faster reopenning")
else:
    # Avoid loosing time unpacking png file for every try
    width, height = 1562, 2424
    data = array("B")
    fp = open("png.array", "rb")
    data.fromfile(fp, width * height * 3)
    fp.close()


raw_output = []

for col in range(height):
    curent_byte = 0 # ! data hiding are pads on column start
    bit = 0
    for row in range(width):
        start = 3 * (row * width + col)
        for color in range(3):
            byte = data[start + color]
            curent_byte *= 2
            curent_byte += byte & 1
            bit += 1
            if bit == 8:
                raw_output.append(curent_byte)
                curent_byte = 0
                bit = 0
    raw_output.append(ord('\n'))
    raw_output.append(ord('\n'))

end = raw_output.index(int('99', 16))

out = open("elf_dump.txt", "wb")
out.write(bytes(raw_output[:end]))
