from itertools import zip_longest


broken = open("elf_dump.txt", "r")
full = open("elf_full.txt", "w")
dummy = "00000000: 0000 0000 0000 0000 0000 0000 0000 0000  ................"


def reconstruct_end(line, prev):
    addr, *data = prev.split(":")
    data = "".join(data)
    addr = int(addr, 16) + 16
    new = f"{addr:08x}"
    for l, d in zip_longest(line[8:].strip(), dummy[8:]):
        if l is not None:
            new += l
        else:
            new += d
    return new, addr


def reconstruct_start(line, prev_addr, next_line):
    lines = []
    reconstruct_line_addr = int(next_line.split(":")[0], 16) - 16
    # todo fill empty lines
    for addr in range(prev_addr + 16, reconstruct_line_addr, 16):
        new = f"{addr:08x}" + dummy[8:]
        lines.append(new)

    new = f"{reconstruct_line_addr:08x}"
    data = [None] * (68 - len(line))
    data += [c for c in line.strip()]
    for l, d in zip_longest(data[8:], dummy[8:]):
        if l is not None:
            new += l
        else:
            new += d
    lines.append(new)
    return lines


missing_start = missing_head = False

for line in broken.readlines():
    if not line.strip():
        missing_start = True
    elif len(line) == 68:
        if missing_head:
            missing_head = False
            block = reconstruct_start(missing_head_line, prev_addr, line)
            for part in block:
                full.write(part + "\n")
            full.write(line)
        else:
            full.write(line)
            last_line = line
    else:
        if missing_start:
            missing_head_line = line
            missing_start = False
            missing_head = True
        else:
            new, prev_addr = reconstruct_end(line, last_line)
            full.write(new + "\n")
